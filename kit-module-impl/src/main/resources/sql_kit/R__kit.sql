CREATE TABLE IF NOT EXISTS kit (
    name       VARCHAR(16) NOT NULL,
    timeunit   SMALLINT    NOT NULL,
    time_value INT         NOT NULL,
    CONSTRAINT pk_kit PRIMARY KEY (name)
);

CREATE TABLE IF NOT EXISTS kit_item (
    id     INT AUTO_INCREMENT NOT NULL,
    kit_id VARCHAR(16)        NOT NULL,
    item   TEXT               NOT NULL,
    CONSTRAINT pk_kit_item PRIMARY KEY (id)
);

ALTER TABLE kit_item ADD CONSTRAINT FK_KIT_ITEM_ON_KIT FOREIGN KEY (kit_id) REFERENCES kit (name);