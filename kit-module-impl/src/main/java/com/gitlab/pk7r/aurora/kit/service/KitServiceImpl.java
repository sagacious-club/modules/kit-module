package com.gitlab.pk7r.aurora.kit.service;

import com.github.stefvanschie.inventoryframework.Gui;
import com.github.stefvanschie.inventoryframework.GuiItem;
import com.github.stefvanschie.inventoryframework.pane.PaginatedPane;
import com.github.stefvanschie.inventoryframework.pane.Pane;
import com.github.stefvanschie.inventoryframework.pane.StaticPane;
import com.gitlab.pk7r.aurora.configuration.FileConfiguration;
import com.gitlab.pk7r.aurora.exception.ExecutionException;
import com.gitlab.pk7r.aurora.execution.Execution;
import com.gitlab.pk7r.aurora.kit.model.Kit;
import com.gitlab.pk7r.aurora.kit.model.KitItem;
import com.gitlab.pk7r.aurora.kit.repository.KitRepository;
import com.gitlab.pk7r.aurora.service.DelayService;
import com.gitlab.pk7r.aurora.util.Message;
import lombok.AllArgsConstructor;
import lombok.val;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import javax.transaction.Transactional;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class KitServiceImpl implements KitService {

    FileConfiguration fileConfiguration;
    
    KitRepository kitRepository;
    
    Plugin plugin;
    
    Execution execution;
    
    DelayService delayService;
    
    @Override
    public List<Kit> getAllKits() {
        return kitRepository.findAll();
    }
    
    @Override
    public Optional<Kit> getKit(String name) {
        return kitRepository.findById(name.toLowerCase());
    }
    
    @Override
    public void deleteKit(final Player player, final String name) {
        execution.run(() -> {
            if (getKit(name).isEmpty()) {
                throw new ExecutionException(player, "Este kit n\u00e3o existe.");
            }
            final Kit kit = getKit(name).get();
            kitRepository.delete(kit);
            return kit;
        });
    }
    
    @Transactional
    @Override
    public void accessKit(final Player player, final String name) {
        execution.run(() -> {
            if (getKit(name).isEmpty()) {
                throw new ExecutionException(player, "Este kit n\u00e3o existe.");
            }
            final Kit kit = getKit(name).get();
            if (!delayService.assertDelay(player, String.format("kit-%s", kit.getName()), (long)kit.getTime(), kit.getTimeunit())) {
                giveKit(player, kit.getName());
            }
            return kit;
        });
    }
    
    @Override
    public void giveKit(final Player player, final String name) {
        execution.run(() -> {
            if (getKit(name).isEmpty()) {
                throw new ExecutionException(player, "Este kit não existe.");
            }
            final Kit kit = getKit(name).get();
            kit.getItems().forEach(item -> player.getInventory().addItem(new ItemStack[] { convertToEntityAttribute(item.getItem()) }));
            return kit;
        });
    }
    
    @Override
    public void previewKit(final Player player, final String name) {
        ConfigurationSection configSection = fileConfiguration.getConfiguration("kit.yml").getConfigurationSection("menu");
        if (getKit(name).isEmpty()) {
            player.spigot().sendMessage(new Message("Este kit não existe.").formatted());
            return;
        }
        Kit kit = getKit(name).get();
        Gui gui = new Gui(plugin, 6, String.format("Kit %s", kit.getName()));

        StaticPane pane = new StaticPane(0, 0, 9, 6);
        assert configSection != null;
        pane.fillWith(getCustomItem(Objects.requireNonNull(configSection.getConfigurationSection("fill"))),
                event -> event.setCancelled(true));
        gui.addPane(pane);

        PaginatedPane contentPane = new PaginatedPane(1, 1, 7, 4);
        List<GuiItem> contents = new ArrayList<>();
        kit.getItems().stream()
                .filter(Objects::nonNull)
                .forEach(item -> contents
                        .add(new GuiItem(convertToEntityAttribute(item.getItem()), event -> event.setCancelled(true))));
        contentPane.populateWithGuiItems(contents);
        gui.addPane(contentPane);

        StaticPane accessKit = new StaticPane(4, 5, 1, 1, Pane.Priority.HIGH);
        if (player.hasPermission("aurora.kit.access." + kit.getName()))
            if (delayService.isInDelay(player, String.format("kit-%s", kit.getName())))
                accessKit.addItem(new GuiItem(
                        getCustomItem(Objects.requireNonNull(configSection.getConfigurationSection("cooldown-kit"))),
                        event -> {
                            event.setCancelled(true);
                            event.getWhoClicked().closeInventory();
                        }), 0, 0);
            else if (getEmptySlotsInInventory(player) < kit.getItems().size()) {
                val item = new GuiItem(getCustomItem(Objects.requireNonNull(configSection.getConfigurationSection("no-space-inventory"))), inventoryClickEvent -> {
                    inventoryClickEvent.setCancelled(true);
                    inventoryClickEvent.getWhoClicked().closeInventory();
                });
                accessKit.addItem(item, 0 ,0);
            } else {
                val guiItem = new GuiItem(getCustomItem(Objects.requireNonNull(configSection.getConfigurationSection("no-space-inventory"))), inventoryClickEvent -> {
                    inventoryClickEvent.setCancelled(true);
                    inventoryClickEvent.getWhoClicked().closeInventory();

                    delayService.assertDelay(player,
                            String.format("kit-%s", kit.getName()), kit.getTime(), kit.getTimeunit());
                    kit.getItems().forEach(item -> player.getInventory()
                            .addItem(convertToEntityAttribute(item.getItem())));
                    player.sendMessage(
                            ChatColor.translateAlternateColorCodes('&',
                                    String.format("&aO kit &e%s &afoi entregue com sucesso.", kit.getName())));
                });
                accessKit.addItem(guiItem, 0, 0);
            }
        else
            accessKit.addItem(new GuiItem(
                    getCustomItem(Objects.requireNonNull(configSection.getConfigurationSection("no-permission-kit"))),
                    event -> {
                        event.setCancelled(true);
                        event.getWhoClicked().closeInventory();
                    }), 0, 0);
        gui.addPane(accessKit);

        StaticPane back = new StaticPane(3, 5, 1, 1, Pane.Priority.HIGH);
        StaticPane forward = new StaticPane(5, 5, 1, 1, Pane.Priority.HIGH);

        back.addItem(new GuiItem(
                getCustomItem(Objects.requireNonNull(configSection.getConfigurationSection("back-page"))),
                event -> {
                    contentPane.setPage(contentPane.getPage() - 1);

                    if (contentPane.getPage() == 0) back.setVisible(false);

                    forward.setVisible(true);
                    gui.update();
                }), 0, 0);
        back.setVisible(false);

        forward.addItem(new GuiItem(
                getCustomItem(Objects.requireNonNull(configSection.getConfigurationSection("forward-page"))),
                event -> {
                    contentPane.setPage(contentPane.getPage() + 1);

                    if (contentPane.getPage() == contentPane.getPages() - 1) forward.setVisible(false);

                    back.setVisible(true);
                    gui.update();
                }), 0, 0);
        if (contentPane.getPage() == contentPane.getPages() - 1) forward.setVisible(false);

        gui.addPane(back);
        gui.addPane(forward);

        gui.show(player);
    }
    
    @Override
    public void createKit(final Player player, final String name, final TimeUnit timeUnit, final Integer time) {
        execution.run(() -> {
            if (!validate(name)) {
                throw new ExecutionException(player, "Nome inválido.");
            }
            if (isInventoryEmpty(player, true)) {
                throw new ExecutionException(player, "Você precisa ter um item no seu inventário para criar um kit.");
            }
            if (getKit(name).isPresent()) {
                throw new ExecutionException(player, "Este kit já existe.");
            }
            Kit kit = new Kit();
            kit.setName(name.toLowerCase());
            kit.setTimeunit(timeUnit);
            kit.setTime(time);
            Arrays.stream(player.getInventory().getContents())
                    .filter(Objects::nonNull)
                    .filter(item -> !Material.AIR.equals(item.getType()))
                    .forEach(item -> {
                        KitItem kitItem = new KitItem();
                        kitItem.setKit(kit);
                        kitItem.setItem(convertToDatabaseColumn(item.clone()));
                        kit.getItems().add(kitItem);
                    });
            player.getInventory().clear();
            return kitRepository.save(kit);
        });
    }
    
    @Override
    public boolean isInventoryEmpty(final Player player, final boolean ignoreArmor) {
        PlayerInventory inventory = player.getInventory();
        Stream<ItemStack> stream = Arrays.stream(inventory.getContents());
        if (!ignoreArmor) {
            stream = Stream.concat(stream, Arrays.stream(inventory.getArmorContents()));
        }
        return stream.filter(Objects::nonNull).allMatch(item -> Material.AIR.equals(item.getType()));
    }
    
    @Override
    public boolean validate(final String kitName) {
        if (kitName.length() > 16) {
            return false;
        }
        final Pattern PATTTERN = Pattern.compile("[^a-zA-Z0-9_]+");
        final Matcher matcher = PATTTERN.matcher(kitName);
        return !matcher.find();
    }
    
    @Override
    public String convertToDatabaseColumn(final ItemStack item) {
        try {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            final BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);
            dataOutput.writeObject(item.clone());
            dataOutput.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        }
        catch (Throwable ignored) {
            return null;
        }
    }
    
    @Override
    public ItemStack convertToEntityAttribute(final String data) {
        try {
            final ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
            final BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
            final ItemStack item = (ItemStack)dataInput.readObject();
            dataInput.close();
            return item.clone();
        }
        catch (Throwable ignored) {
            return null;
        }
    }
    
    @Override
    public int getEmptySlotsInInventory(final Player player) {
        return (int) Arrays.stream(player.getInventory().getStorageContents()).filter(item -> item == null || item.getType() == Material.AIR).count();
    }
    
    @Override
    public ItemStack getCustomItem(final Material material, final byte data, final String name, final List<String> description) {
        ItemStack itemStack = new ItemStack(material, 1, data);
        ItemMeta itemMeta = itemStack.getItemMeta();

        assert itemMeta != null;
        itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        if (description != null)
            itemMeta.setLore(description.stream()
                    .map(str -> ChatColor.translateAlternateColorCodes('&', str))
                    .collect(Collectors.toList()));

        itemStack.setItemMeta(itemMeta);
        return itemStack;

    }
    
    @Override
    public ItemStack getCustomItem(final ConfigurationSection configurationSection) {
        final String[] material = Objects.requireNonNull(configurationSection.getString("material")).split(":");
        byte data = 0;
        if (material.length > 1) {
            data = Byte.parseByte(material[1]);
        }
        return getCustomItem(Material.getMaterial(material[0]), data, configurationSection.getString("display-name"), configurationSection.getStringList("lore"));
    }
}
