package com.gitlab.pk7r.aurora.kit.command;

import com.gitlab.pk7r.aurora.kit.service.KitService;
import com.gitlab.pk7r.aurora.util.Message;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import javax.annotation.PostConstruct;
import dev.jorel.commandapi.CommandAPICommand;
import com.gitlab.pk7r.aurora.Command;

@Command
public class KitsCommand {
    KitService kitService;
    
    @PostConstruct
    public void kitsCommand() {
        new CommandAPICommand("kits")
                .executesPlayer((player, args) -> {
            final String message = this.kitService.getAllKits().stream()
                    .filter(kit -> player.hasPermission("aurora.kit.access." + kit.getName().toLowerCase()))
                    .map(kit -> String.format("&e[%s](/kit %s hover=&6Kit %s)", kit.getName(), kit.getName(), kit.getName()))
                            .collect(Collectors.collectingAndThen(Collectors.joining("&6, &e"), result ->
                                    result.isEmpty() ? "&cNenhum kit disponível." : result));
            player.spigot().sendMessage(new Message(message).formatted());
        }).register();
    }
    
    @Autowired
    public KitsCommand(final KitService kitService) {
        this.kitService = kitService;
    }
}
