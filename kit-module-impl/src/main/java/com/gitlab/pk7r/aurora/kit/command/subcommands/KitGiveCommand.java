package com.gitlab.pk7r.aurora.kit.command.subcommands;

import com.gitlab.pk7r.aurora.kit.model.Kit;
import com.gitlab.pk7r.aurora.kit.service.KitService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.GreedyStringArgument;

public class KitGiveCommand {

    public static CommandAPICommand getCommand(KitService kitService) {
        return new CommandAPICommand("coletar")
                .withArguments(new GreedyStringArgument("nome").replaceSuggestions(info -> kitService.getAllKits().stream().map(Kit::getName).filter(name ->
                        info.sender().hasPermission(String.format("aurora.kit.access.%s", name))).toArray(String[]::new)))
        .executesPlayer((player, args) -> {
            if (!player.hasPermission("aurora.kit.command.coletar")) {
                CommandAPI.fail("Sem permissão.");
                return;
            }
            final String kitName = (String) args[0];
            kitService.giveKit(player, kitName);
            player.spigot().sendMessage(new Message(String.format("&aKit &e%s &acoletado com sucesso.", kitName)).formatted());
        });
    }
    
    public static CommandAPICommand getInvalidSyntax() {
        return new CommandAPICommand("coletar").executesPlayer((player, args) -> {
            if (!player.hasPermission("aurora.kit.command.coletar")) {
                CommandAPI.fail("Sem permissão.");
                return;
            }
            CommandAPI.fail("Uso correto: /kit coletar <nome>");
        });
    }
}
