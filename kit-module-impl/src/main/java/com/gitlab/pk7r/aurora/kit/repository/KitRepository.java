package com.gitlab.pk7r.aurora.kit.repository;

import org.springframework.stereotype.Repository;
import com.gitlab.pk7r.aurora.kit.model.Kit;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface KitRepository extends JpaRepository<Kit, String> {

}