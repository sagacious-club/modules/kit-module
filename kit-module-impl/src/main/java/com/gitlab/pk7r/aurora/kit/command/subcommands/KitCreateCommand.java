package com.gitlab.pk7r.aurora.kit.command.subcommands;

import com.gitlab.pk7r.aurora.kit.service.KitService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.GreedyStringArgument;
import dev.jorel.commandapi.arguments.StringArgument;

import java.util.concurrent.TimeUnit;

public class KitCreateCommand {

    public static CommandAPICommand getCommand(KitService kitService) {
        return new CommandAPICommand("criar").withArguments(new StringArgument("tempo"),
                new GreedyStringArgument("nome")).executesPlayer((player, args) -> {
            if (!player.hasPermission("aurora.kit.command.criar")) {
                CommandAPI.fail("Sem permissão.");
                return;
            }
            final String kitName = (String)args[1];
            final String time = (String)args[0];
            TimeUnit timeUnit;
            if (time.endsWith("s")) {
                timeUnit = TimeUnit.SECONDS;
            }
            else if (time.endsWith("m")) {
                timeUnit = TimeUnit.MINUTES;
            }
            else if (time.endsWith("h")) {
                timeUnit = TimeUnit.HOURS;
            }
            else {
                if (!time.endsWith("d")) {
                    player.spigot().sendMessage(new Message("&cUnidade de tempo inv\u00e1lida.").formatted());
                    return;
                }
                timeUnit = TimeUnit.DAYS;
            }
            kitService.createKit(player, kitName, timeUnit, Integer.parseInt(time.substring(0, time.length() - 1)));
        });
    }
    
    public static CommandAPICommand getInvalidSyntax() {
        return new CommandAPICommand("criar").withArguments(new StringArgument("tempo")).executesPlayer((player, args) -> {
            if (!player.hasPermission("aurora.kit.command.criar")) {
                CommandAPI.fail("Sem permissão.");
                return;
            }
            CommandAPI.fail("Uso correto: /kit criar <tempo> <nome>");
        });
    }
    
    public static CommandAPICommand getInvalidSyntaxNoArgs() {
        return new CommandAPICommand("criar").executesPlayer((player, args) -> {
            if (!player.hasPermission("aurora.kit.command.criar")) {
                CommandAPI.fail("Sem permissão.");
                return;
            }
            CommandAPI.fail("Uso correto: /kit criar <tempo> <nome>");
        });
    }
}
