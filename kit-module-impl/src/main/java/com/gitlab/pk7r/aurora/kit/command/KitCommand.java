package com.gitlab.pk7r.aurora.kit.command;

import com.gitlab.pk7r.aurora.Command;
import com.gitlab.pk7r.aurora.kit.command.subcommands.KitCreateCommand;
import com.gitlab.pk7r.aurora.kit.command.subcommands.KitDeleteCommand;
import com.gitlab.pk7r.aurora.kit.command.subcommands.KitGiveCommand;
import com.gitlab.pk7r.aurora.kit.model.Kit;
import com.gitlab.pk7r.aurora.kit.service.KitService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.GreedyStringArgument;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@Command
public class KitCommand {

    KitService kitService;
    
    @PostConstruct
    public void kitCommand() {
        new CommandAPICommand("kit")
                .executesPlayer((player, args) -> {
                    player.spigot().sendMessage(new Message("&cUso correto: /kit <nome>").formatted());
                }).register();
        new CommandAPICommand("kit")
                .withArguments(new GreedyStringArgument("nome").replaceSuggestions(info -> kitService.getAllKits().stream().map(Kit::getName).filter(name ->
                        info.sender().hasPermission(String.format("aurora.kit.access.%s", name))).toArray(String[]::new)))
        .executesPlayer((player, args) -> {
            final String kitName = (String)args[0];
            if (!player.hasPermission(String.format("aurora.kit.access.%s", kitName))) {
                CommandAPI.fail("Sem permissão.");
                return;
            }
            kitService.previewKit(player, kitName);
        })
                .withSubcommand(KitGiveCommand.getCommand(kitService))
                .withSubcommand(KitGiveCommand.getInvalidSyntax())
                .withSubcommand(KitCreateCommand.getCommand(kitService))
                .withSubcommand(KitCreateCommand.getInvalidSyntax())
                .withSubcommand(KitCreateCommand.getInvalidSyntaxNoArgs())
                .withSubcommand(KitDeleteCommand.getCommand(kitService))
                .withSubcommand(KitDeleteCommand.getInvalidSyntax()).register();
    }
    
    @Autowired
    public KitCommand(final KitService kitService) {
        this.kitService = kitService;
    }
}