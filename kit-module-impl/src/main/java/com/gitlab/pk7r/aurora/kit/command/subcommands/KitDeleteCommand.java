package com.gitlab.pk7r.aurora.kit.command.subcommands;

import com.gitlab.pk7r.aurora.kit.service.KitService;
import com.gitlab.pk7r.aurora.util.Message;
import dev.jorel.commandapi.CommandAPI;

import java.util.function.Function;
import com.gitlab.pk7r.aurora.kit.model.Kit;
import dev.jorel.commandapi.arguments.GreedyStringArgument;
import dev.jorel.commandapi.arguments.Argument;
import dev.jorel.commandapi.CommandAPICommand;

public class KitDeleteCommand {

    public static CommandAPICommand getCommand(KitService kitService) {
        return new CommandAPICommand("deletar")
                .withArguments(new GreedyStringArgument("nome").replaceSuggestions(info -> kitService.getAllKits().stream().map(Kit::getName).filter(name ->
                         info.sender().hasPermission(String.format("aurora.kit.access.%s", name))).toArray(String[]::new)))
                .executesPlayer((player, args) -> {
            if (!player.hasPermission("aurora.kit.command.deletar")) {
                CommandAPI.fail("Sem permissão.");
                return;
            }
            final String kitName = (String)args[0];
            kitService.deleteKit(player, kitName);
            player.spigot().sendMessage(new Message(String.format("&aKit &e%s &adeletado com sucesso.", kitName)).formatted());
        });
    }
    
    public static CommandAPICommand getInvalidSyntax() {
        return new CommandAPICommand("deletar").executesPlayer((player, args) -> {
            if (!player.hasPermission("aurora.kit.command.deletar")) {
                CommandAPI.fail("Sem permissão.");
                return;
            }
            CommandAPI.fail("Uso correto: /kit deletar <nome>");
        });
    }
}
