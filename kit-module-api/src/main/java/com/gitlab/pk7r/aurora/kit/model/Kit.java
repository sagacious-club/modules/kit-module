package com.gitlab.pk7r.aurora.kit.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Getter
@Setter
@Entity
@Table(name = "kit")
public class Kit {

    @Id
    @Column(name = "name", length = 16, nullable = false, updatable = false)
    private String name;

    @Enumerated
    @Column(name = "timeunit", nullable = false, columnDefinition = "smallint")
    private TimeUnit timeunit;

    @Column(name = "time_value", nullable = false, columnDefinition = "int")
    private Integer time;

    @OneToMany(mappedBy = "kit", cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<KitItem> items = new ArrayList<>();
}