package com.gitlab.pk7r.aurora.kit.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "kit_item")
public class KitItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "kit_id", nullable = false, updatable = false)
    private Kit kit;

    @Column(name = "item", nullable = false, updatable = false, columnDefinition = "text")
    private String item;

}