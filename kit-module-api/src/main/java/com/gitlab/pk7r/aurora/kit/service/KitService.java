package com.gitlab.pk7r.aurora.kit.service;

import com.gitlab.pk7r.aurora.kit.model.Kit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import com.github.stefvanschie.inventoryframework.gui.type.ChestGui;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public interface KitService {

    List<Kit> getAllKits();

    Optional<Kit> getKit(String name);

    void deleteKit(Player player, String name);

    void accessKit(Player player, String name);

    void giveKit(Player player, String name);

    void previewKit(Player player, String name);

    void createKit(Player player, String name, TimeUnit timeUnit, Integer time);

    boolean isInventoryEmpty(Player player, boolean ignoreArmor);

    boolean validate(String kitName);

    String convertToDatabaseColumn(ItemStack item);

    ItemStack convertToEntityAttribute(String data);

    int getEmptySlotsInInventory(Player player);

    ItemStack getCustomItem(Material material, byte data, String name, List<String> description);

    ItemStack getCustomItem(ConfigurationSection configurationSection);

}